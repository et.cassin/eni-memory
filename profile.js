window.onload = init;

let imageContainer;
const extensions = new Map([
    ['alphabet-scrabble', { 'ext': '.png', 'nb': 26 }],
    ['animaux', { 'ext': '.webp', 'nb': 27 }],
    ['animauxAnimes', { 'ext': '.webp', 'nb': 8 }],
    ['animauxdomestiques', { 'ext': '.jpg', 'nb': 10 }],
    ['chiens', { 'ext': '.webp', 'nb': 23 }],
    ['dinosaures', { 'ext': '.jpg', 'nb': 10 }],
    ['dinosauresAvecNom', { 'ext': '.jpg', 'nb': 10 }],
    ['memory-legume', { 'ext': '.svg', 'nb': 6 }],
]);

function init() {
    updateScores();
    imageContainer = document.getElementById('images');

    let saved = localStorage.getItem('pref');
    if (saved) {
        saved = JSON.parse(saved);
        updateDisplay(saved.name, extensions.get(saved.name));
        updateSelect(saved.name);
    } else {
        updateDisplay('alphabet-scrabble', extensions.get('alphabet-scrabble'));
    }
    updateTaille();

    let user = sessionStorage.getItem('user');
    if (user) {
        user = JSON.parse(user);
        let nameElem = document.getElementById('user');
        let mailElem = document.getElementById('mail');
        nameElem.value = user.name;
        mailElem.value = user.mail;
    }

    document.getElementById('choixTaille').addEventListener('change', choisirTaille);
    document.getElementById('choixMemory').addEventListener('change', choisirMemory);
}

function choisirMemory(event) {
    let choix = event.currentTarget.value;
    let options = extensions.get(choix);

    updateDisplay(choix, options);

    let preference = {
        'name': choix,
        'ext': options.ext,
        'size': options.nb
    };

    localStorage.setItem('pref', JSON.stringify(preference));
}

function choisirTaille(event) {
    localStorage.setItem('taille', event.currentTarget.value);
}

function updateDisplay(choix, options) {
    let path = 'images/' + choix + '/';

    imageContainer.innerHTML = '';
    for (let i = 1; i <= options.nb; i++) {
        let img = document.createElement('img');
        img.src = path + i + options.ext;
        img.className = 'icone';

        imageContainer.appendChild(img);
    }
}

function updateSelect(choix) {
    let options = document.getElementById('choixMemory').querySelectorAll('option');
    options.forEach(opt => {
        if (opt.value === choix) opt.setAttribute('selected', '');
    });
}

function updateTaille() {
    let savedSize = localStorage.getItem('taille');
    if (!savedSize) savedSize = '2x4';

    let tailles = document.getElementById('choixTaille').querySelectorAll('option');

    tailles.forEach(opt => {
        if (opt.value === savedSize) opt.setAttribute('selected', '');
    });
}