window.onload = init;
let boutonValider;

function init() {
    boutonValider = document.querySelector('input[type="submit"]');
    document.forms[0].addEventListener('submit', connexion);
}

function connexion(event) {
    event.preventDefault();

    let name = document.getElementById('user').value;
    let pass = document.getElementById('password').value;

    let user = {
        "name" : name,
        "pass" : pass
    }
    
    let users = localStorage.getItem('users');
    if (users) {
        let usersObj = JSON.parse(users);

        usersObj.forEach(element => {
            if (element.name === user.name && element.pass === user.pass) {
                user.mail = element.mail;
                sessionStorage.setItem('user', JSON.stringify(user));
                location.href="./jeu.html";
                return;
            }
        });
    }

    alert("Les identifiants saisis ne permettent pas de vous authentifier.");
}
