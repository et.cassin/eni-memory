

function updateScores() {
    let scoreContainer = document.getElementById('scores');

    let scores = localStorage.getItem('parties');
    let user = sessionStorage.getItem('user');
    if (user) {
        user = JSON.parse(user).name;
    } else {
        return;
    }
    if (scores) {
        scores = JSON.parse(scores);
        
        scores = scores.filter(a => a.user === user);

        for (let i = 0; i < scores.length; i++) {
            let tr = document.createElement('tr');
            
            let tdpseudo = document.createElement('td');
            tdpseudo.innerText = scores[i].user;

            let tdscore = document.createElement('td');
            tdscore.innerText = scores[i].score;

            let tdtaille = document.createElement('td');
            tdtaille.innerText = scores[i].taille;

            let tdmemory = document.createElement('td');
            tdmemory.innerText = scores[i].memory;

            let tddate = document.createElement('td');
            tddate.innerText = scores[i].date;

            tr.appendChild(tdpseudo);
            tr.appendChild(tdscore);
            tr.appendChild(tdtaille);
            tr.appendChild(tdmemory);
            tr.appendChild(tddate);

            scoreContainer.appendChild(tr);
        }
    }
}