let cards;
let foundPairs = 0;
let buffer = [];
let prefs;
let taille;
let score = 0;
const ROTATION_SPEED = 250;
const VISIBLE_TIME = 2400;

window.onload = init;

function init() {
    updateScores();
    prefs = localStorage.getItem('pref');
    if (prefs) {
        prefs = JSON.parse(prefs);
    } else {
        prefs = {'name':'animaux', 'ext':'.webp'}
    }

    chargerGrille();

    document.addEventListener('keypress', (event) => {
        if (event.key === ' ') {
            chargerGrille();
        }
    });
}

function chargerGrille() {
    taille = localStorage.getItem('taille');
    if (!taille) {
        taille = '2x4';
    }

    let height = parseInt(taille.charAt(0));
    let width = parseInt(taille.charAt(2));
    let maxCardValue = height * width / 2;

    let board = document.getElementById('board');
    board.innerHTML = '';
    foundPairs = 0;
    buffer = [];
    cards = new Array();

    for (let i = 0; i < height; i++) {
        let tr = document.createElement('tr');
        for (let j = 0; j < width; j++) {
            let td = document.createElement('td');
            let img = document.createElement('img');
            img.classList.add('hidden-card');
            img.setAttribute('src', 'images/question.svg');
            img.id = i * width + j;

            img.addEventListener('click', turn);

            td.appendChild(img);
            tr.appendChild(td);

            let tirage = Math.trunc(Math.random() * maxCardValue) + 1;
            while (cards.filter(v => v === tirage).length === 2) {
                tirage = Math.trunc(Math.random() * maxCardValue) + 1;
            }
            cards.push(tirage);
        }
        board.appendChild(tr);
    }
}

function turn(event) {
    if (buffer.length == 2) return;

    let elem = event.currentTarget;
    elem.removeEventListener('click', turn);
    elem.animate(
        [{transform: 'scaleX(1)'}, {transform: 'scaleX(0)'}],
        {duration: ROTATION_SPEED})
        .onfinish = () => {
            elem.src = 'images/' + prefs.name + '/' + cards[elem.id] + prefs.ext;
            elem.animate(
                [{transform: 'scaleX(0)'}, {transform: 'scaleX(1)'}],
                {duration: ROTATION_SPEED});
        };
    buffer.push(elem);
    if (buffer.length == 2) {
        score++;
        if (cards[buffer[0].id] !== cards[buffer[1].id]) {
            setTimeout(() => {
                buffer.forEach(b => {
                    b.animate(
                        [{transform: 'scaleX(1)'}, {transform: 'scaleX(0)'}],
                        {duration: ROTATION_SPEED})
                        .onfinish = () => {
                            b.src = 'images/question.svg';
                            b.addEventListener('click', turn);
                            b.animate(
                                [{transform: 'scaleX(0)'}, {transform: 'scaleX(1)'}],
                                {duration: ROTATION_SPEED});
                        };
                });
                buffer = [];
            }, VISIBLE_TIME);
        } else {
            buffer = [];
            foundPairs++;
            if (foundPairs * 2 === cards.length) {
                victoire();
            }
        }
    }
}

function victoire() {
    let games = localStorage.getItem('parties');
    if (games) {
        games = JSON.parse(games);
    } else {
        games = [];
    }

    let user = sessionStorage.getItem('user');
    if (user) {
        user = JSON.parse(user).name;
    } else {
        user = 'unknown';
    }

    let game = {
        'user': user,
        'score': score,
        'taille': taille,
        'memory': prefs.name,
        'date': new Date().toLocaleDateString()
    };

    games.push(game);
    localStorage.setItem('parties', JSON.stringify(games));

    document.getElementById('victoire').style.visibility = 'visible';
    document.getElementById('victoire').animate(
        [{transform: 'translate(-50%, -100%) scale(0,0)'}, {transform: 'translate(-50%, -100%) scale(1,1)'}],
        {duration: 800}
    ).onfinish = () => {
        setTimeout(() => {
            document.getElementById('victoire').animate(
                [{transform: 'translate(-50%, -100%) scale(1,1)'}, {transform: 'translate(-50%, -100%) scale(0,0)'}],
                {duration: 400}
            ).onfinish = () => {
                document.getElementById('victoire').style.visibility = 'hidden';
            };
        }, 2000);
    };
}