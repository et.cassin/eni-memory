

function updateScores() {
    let scoreContainer = document.getElementById('scores');

    let scores = localStorage.getItem('parties');
    if (scores) {
        scores = JSON.parse(scores);
        let length = scores.length;
        if (length > 5) {
            length = 5;
        }
        scores.sort((a, b) => a.score - b.score);

        for (let i = 0; i < length; i++) {
            let tr = document.createElement('tr');
            
            let tdpseudo = document.createElement('td');
            tdpseudo.innerText = scores[i].user;

            let tdscore = document.createElement('td');
            tdscore.innerText = scores[i].score;

            let tdtaille = document.createElement('td');
            tdtaille.innerText = scores[i].taille;

            let tdmemory = document.createElement('td');
            tdmemory.innerText = scores[i].memory;

            let tddate = document.createElement('td');
            tddate.innerText = scores[i].date;

            tr.appendChild(tdpseudo);
            tr.appendChild(tdscore);
            tr.appendChild(tdtaille);
            tr.appendChild(tdmemory);
            tr.appendChild(tddate);

            scoreContainer.appendChild(tr);
        }
    }
}