window.onload = init;
let userOK = false;
let mailOK = false;
let passwordOK = false;
let boutonValider;
let passHints;

function init() {
    boutonValider = document.querySelector('input[type="submit"]');
    passHints = document.querySelectorAll('.pass-hint div');
    updateHint(0);
    updateBouton();
    document.getElementById('user').addEventListener('input', validerUser);
    document.getElementById('mail').addEventListener('input', validerMail);
    document.getElementById('password').addEventListener('input', validerPass);
    document.forms[0].addEventListener('submit', enregistrerUser);
}

function enregistrerUser(event) {
    event.preventDefault();

    if (!(userOK && mailOK && passwordOK)) {
        document.body.animate(
            [{transform: 'scale(1,1)'}, {transform: 'scale(1.2,1.2)'}],
            {duration: 400});
        return;
    }

    let name = document.getElementById('user').value;
    let mail = document.getElementById('mail').value;
    let pass = document.getElementById('password').value;
    let user = {
        "name" : name,
        "mail" : mail,
        "pass" : pass
    };

    let users = localStorage.getItem('users');
    if (users) {
        let usersObj = JSON.parse(users);

        usersObj.forEach(element => {
            if (element.mail === user.mail) {
                alert('Cette adresse mail est déjà utilisée.');
                return;
            }
        });

        usersObj.push(user);
        localStorage.setItem('users', JSON.stringify(usersObj));
    } else {
        let usersObj = [];
        usersObj.push(user);
        localStorage.setItem('users', JSON.stringify(usersObj));
    }
    location.href="./connexion.html";
}

function validerUser(event) {
    if (event.currentTarget.value.length > 2) {
        event.currentTarget.className = 'valide';
        document.getElementById('userHint').src = 'images/check.svg';
        userOK = true;
    } else {
        event.currentTarget.className = 'pas-valide';
        document.getElementById('userHint').src = 'images/error.svg';
        userOK = false;
    }
    updateBouton();
}

function validerMail(event) {
    if (event.currentTarget.value.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$/g)) {
        event.currentTarget.className = 'valide';
        document.getElementById('mailHint').src = 'images/check.svg';
        mailOK = true;
    } else {
        event.currentTarget.className = 'pas-valide';
        document.getElementById('mailHint').src = 'images/error.svg';
        mailOK = false;
    }
    updateBouton();
}

function validerPass(event) {
    let chiffreOK = /\d/g.test(event.currentTarget.value);
    let symboleOK = /\W/g.test(event.currentTarget.value);
    let lettreOK = /[a-zA-Z]/g.test(event.currentTarget.value);
    let tailleOK = event.currentTarget.value.length > 5;

    let nbConditions = (chiffreOK ? 1 : 0)
                        + (symboleOK ? 1 : 0)
                        + (tailleOK ? 1 : 0);
    updateHint(nbConditions);

    if (chiffreOK && lettreOK && symboleOK && tailleOK) {
        event.currentTarget.className = 'valide';
        document.getElementById('passHint').src = 'images/check.svg';
        passwordOK = true;
    } else {
        event.currentTarget.className = 'pas-valide';
        document.getElementById('passHint').src = 'images/error.svg';
        passwordOK = false;
    }
    updateBouton();
}

function updateBouton() {
    if (userOK && mailOK && passwordOK) {
        boutonValider.disabled = false;
    } else {
        boutonValider.disabled = true;
    }
}

function updateHint(nb) {
    for (let i = 0; i < passHints.length; i++) {
        if (i < nb) {
            passHints[i].style.visibility = 'visible';
        } else {
            passHints[i].style.visibility = 'hidden';
        }
    }
}